//
// Created by crow on 26/11/18.
//

#ifndef LORA_GROUND_CONTROL_DRONELISTMODEL_H
#define LORA_GROUND_CONTROL_DRONELISTMODEL_H

#include "DroneItem.hpp"
class QQmlContext;
#include <QAbstractListModel>

struct Data
{
    QGeoCoordinate coord;
    int angle;
};
Q_DECLARE_METATYPE(Data)


class DroneListModel : public QAbstractListModel {
    Q_OBJECT

public:
    explicit DroneListModel(QObject *parent=nullptr);
    enum Roles{
        NameRole = Qt::UserRole + 1,
        PositionRole,
        HistoryRole,
        AngleRole,
        ColorRole
    };

    void register_objects(const QString &droneName, QQmlContext *context);

    Q_INVOKABLE bool addOrUpdateDrone(QGeoCoordinate coord, int angle, const QString &name);
    Q_INVOKABLE bool createDrone(QGeoCoordinate coord, const QColor &color, const QString &name);

    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    QHash<int, QByteArray> roleNames() const override;
    bool setData(const QModelIndex &index, const QVariant &value, int role) override;
private:
    QList<DroneItem> _drones;
};


#endif //LORA_GROUND_CONTROL_DRONELISTMODEL_H
