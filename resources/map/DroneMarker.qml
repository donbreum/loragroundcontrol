
import QtQuick 2.4
import QtLocation 5.6


// Plane.qml
MapQuickItem {
    id: drone
    property string name: "";
    property bool followMe: false
    property real transparency: 0.5
    property double heading;
    property int spatialPointHeight: 60

    anchorPoint.x: icon.width/2
    anchorPoint.y: icon.height/2

    visible: true

    sourceItem: Column {
        Image {
            id: icon
            rotation: heading
            // Fade out all icons except for the last one
            opacity: 1 //- drone.transparency
            source: "qrc:/map/drone.png"
            width: spatialPointHeight
            height: spatialPointHeight
            sourceSize.width: spatialPointHeight
            sourceSize.height: spatialPointHeight
        }
        Text
        {
            text: name
            horizontalAlignment: Text.AlignCenter
            font.bold: true
            width: icon.width + 10
        }
    }
}
